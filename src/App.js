import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";
import MainPage from "./containers/MainPage";
import './App.css';


class App extends Component {
  render() {
    return (
        <Switch>
          <Route path="/" exact component={MainPage}/>
        </Switch>
    );
  }
}

export default App;

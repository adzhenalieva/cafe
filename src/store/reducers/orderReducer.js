import {MODAL_SHOW, ORDER_CLOSE, ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "../actions/actionTypes";

const initialState = {
    loading: false,
    ordered: false,
    error: null
};

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_REQUEST:
            return {...state, loading: true};
        case ORDER_SUCCESS:
            return {...state, loading: false, ordered: false};
        case ORDER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case ORDER_CLOSE:
            return {
                ordered: false
            };
        case MODAL_SHOW:
            return {
                ordered: true
            };
        default:
            return state;
    }
};

export default orderReducer;
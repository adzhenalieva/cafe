export const ADD_DISH = 'ADD_DISH';
export const REMOVE_DISH = 'REMOVE_DISH';

export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILURE = 'ORDER_FAILURE';

export const DISH_REQUEST = 'DISH_REQUEST';
export const DISH_SUCCESS = 'DISH_SUCCESS';
export const DISH_FAILURE = 'DISH_FAILURE';

export const ORDER_CLOSE = 'ORDER_CLOSE';
export const MODAL_SHOW = 'MODAL_SHOW';

export const INIT_INGREDIENTS = 'INIT_INGREDIENTS';
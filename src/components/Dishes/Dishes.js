import React from 'react';
import './Dishes.css';

const Dishes = props => {
    return (
        <div className="Dishes">
            <img className="DishesImage" src={props.image} alt="dishes"/>
            <p>{props.name}</p>
            <p>{props.price} KGS</p>
            <button className="DishesButton" onClick={props.onClick}>Add to cart</button>
        </div>
    );
};

export default Dishes;
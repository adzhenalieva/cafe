import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Dishes from "../components/Dishes/Dishes";
import {addDish, fetchDishes, removeDish} from "../store/actions/dishesCombine";

import './MainPage.css';
import Cart from "../components/Cart/Cart";
import CardDishes from "../components/CardDishes/CardDishes";
import Modal from "../components/UI/Modal/Modal";
import {closeOrder, showModal} from "../store/actions/order";
import ContactData from "../components/ConactData/ContactData";

class MainPage extends Component {
    componentDidMount() {
        this.props.fetchDishes();
    }

    render() {
        const dishes = Object.keys(this.props.dishes).map(igKey => (
            <Dishes key={igKey}
                    name={igKey}
                    image={this.props.dishes[igKey].image}
                    price={this.props.dishes[igKey].price}
                    onClick={(name) => this.props.addDish(igKey)}/>
        ));
        const cartDishes = Object.keys(this.props.cart).map((igKey, id) => (
            <CardDishes key={id}
                        name={igKey}
                        number={this.props.cart[igKey]}
                        onClick={(name) => this.props.removeDish(igKey)}
            />
        ));
        return (
            <Fragment>
                <Modal show={this.props.ordered}
                       close={this.props.closeOrder}>
                    <ContactData/>
                </Modal>
                <div className="MainPage">
                    <div className="DishesDiv">
                        {dishes}
                    </div>
                    <div className="Cart">
                        <Cart
                            delivery={this.props.delivery}
                            totalPrice={this.props.totalPrice}
                            onClick={this.props.showModal}
                            disabled={this.props.disabled}>
                            {cartDishes}
                        </Cart>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dc.dishes,
        cart: state.dc.cart,
        delivery: state.dc.delivery,
        totalPrice: state.dc.totalPrice,
        ordered: state.order.ordered,
        disabled: state.dc.disabled
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDishes: () => dispatch(fetchDishes()),
        addDish: (name) => dispatch(addDish(name)),
        closeOrder: () => dispatch(closeOrder()),
        showModal: () => dispatch(showModal()),
        removeDish: (name) => dispatch(removeDish(name))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);


import React, {Component} from 'react';
import './Cart.css';

class Cart extends Component {
    render (){
        return (
            <div>
                {this.props.children}
                <p>Delivery {this.props.delivery}</p>
                <p><strong>Total </strong>{this.props.totalPrice}</p>
                <button onClick={this.props.onClick} disabled={this.props.disabled}>Place Order</button>
            </div>
        );
    }
}
export default Cart;
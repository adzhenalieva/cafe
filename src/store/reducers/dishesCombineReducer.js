import {ADD_DISH, DISH_SUCCESS, INIT_INGREDIENTS, REMOVE_DISH} from "../actions/actionTypes";

const DISHES_PRICES = {
    Fanta: 55,
    Coke: 55,
    Sprite: 55,
    Pizza: 560,
    'Fried chicken': 420,
    Sushi: 580
};
const DISHES_INITIAL = {
    Fanta: 0,
    Coke: 0,
    Sprite: 0,
    Pizza: 0,
    'Fried chicken': 0,
    Sushi: 0
};
const initialState = {
    dishes: [],
    delivery: 150,
    cart: DISHES_INITIAL,
    totalPrice: 150,
    disabled: true
};

const dishesCombineReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISH:
            return {
                ...state,
                cart: {...state.cart, [action.dishName]: state.cart[action.dishName] + 1},
                totalPrice: state.totalPrice + DISHES_PRICES[action.dishName],
                disabled: false
            };
        case REMOVE_DISH:
            if (state.cart[action.dishName] > 0) {
                return {
                    ...state,
                    cart: {...state.cart, [action.dishName]: state.cart[action.dishName] - 1},
                    totalPrice: state.totalPrice - DISHES_PRICES[action.dishName],
                }
            } else if (state.totalPrice === 150) {
                return {
                    ...state, disabled: true
                }
            }
            return state;
        case DISH_SUCCESS:
            return {
                ...state,
                dishes: action.response
            };
        case INIT_INGREDIENTS:
            return {
                ...state,
                cart: {},
                totalPrice: 0
            };
        default:
            return state;
    }
};
export default dishesCombineReducer;
import React, {Component} from 'react';
import Spinner from '../../components/UI/Spinner/Spinner';
import './ContactData.css';
import {connect} from "react-redux";
import {createOrder} from "../../store/actions/order";
import Button from "../UI/Button/Button";


class ContactData extends Component {

    state = {
        name: '',
        email: '',
        street: '',
        postal: '',
    };

    valueChanged = (event) => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };

    orderHandler = (event) => {
        event.preventDefault();
        const orderData = {
            dishes: this.props.dishes,
            price: this.props.totalPrice,
            customer: {
                ...this.state
            }
        };
        this.props.createOrder(orderData, this.props.history)

    };


    render() {
        let form = (<form onSubmit={event => this.orderHandler(event)}>
            <input className="Input" value={this.state.name}
                   onChange={this.valueChanged}
                   type="text" name="name" placeholder="Your Name"/>
            <input className="Input" value={this.state.email}
                   onChange={this.valueChanged}
                   type="email" name="email" placeholder="Your Mail"/>
            <input className="Input" value={this.state.street}
                   onChange={this.valueChanged}
                   type="text" name="street" placeholder="Street"/>
            <input className="Input" value={this.state.postal}
                   onChange={this.valueChanged}
                   type="text" name="postal" placeholder="Postal Code"/>
            <Button btnType="Success">ORDER</Button>
        </form>);

        if (this.props.loading) {
            form = <Spinner/>
        }
        return (
            <div className="ContactData">
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dc.cart,
    totalPrice: state.dc.totalPrice,
    loading: state.order.loading

});

const mapDispatchToProps = dispatch => ({

    createOrder: (orderData, history) => dispatch(createOrder(orderData, history)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactData);
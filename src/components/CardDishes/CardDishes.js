import React from 'react';

const CardDishes = props => {
    return (
        <div>
            <span>{props.name}    </span>
            <span>X{props.number}    </span>
            <button onClick= {props.onClick}>X</button>
        </div>
    );
};

export default CardDishes;
import {ADD_DISH, REMOVE_DISH, DISH_FAILURE, DISH_REQUEST, DISH_SUCCESS, INIT_INGREDIENTS} from "./actionTypes";
import axios from '../../axios-orders';

export const addDish = dishName => ({type: ADD_DISH, dishName});

export const removeDish = dishName => ({type: REMOVE_DISH, dishName});


export const dishRequest = () => ({type: DISH_REQUEST});

export const initIngredients = () => ({type: INIT_INGREDIENTS});

export const dishSuccess = response => ({type: DISH_SUCCESS, response});

export const dishFailure = error => ({type: DISH_FAILURE, error});

export const fetchDishes = () => {
    return dispatch => {
        dispatch(dishRequest());
        axios.get('/dishes.json').then(response => {
            dispatch(dishSuccess(response.data));
        }, error => {
            dispatch(dishFailure());
        });
    }
};


import {MODAL_SHOW, ORDER_CLOSE, ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./actionTypes";
import axios from '../../axios-orders';
import {initIngredients} from "./dishesCombine";

export const closeOrder = () => ({type: ORDER_CLOSE});
export const showModal = () => ({type: MODAL_SHOW});

export const orderRequest = () => ({type: ORDER_REQUEST});

export const orderSuccess = () => ({type: ORDER_SUCCESS});

export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const createOrder = (order, history) => {
    return dispatch => {
        dispatch(orderRequest());
         axios.post('/dishesOrders.json', order).then(
             response => {
                 dispatch(orderSuccess());
                 dispatch(initIngredients());
                 history.push('/');
             },
            error => dispatch(orderFailure(error))
        );
    }
};